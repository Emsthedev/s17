console.log(" ")
console.log(" Hello World")
console.log(" Hello World")

/* 
FUNCTIONS
functions are lines/ block of codes that tell pur device/aplication to perform
certain tasks when called/invoked

- functions are mostly created to create complicated tasks to run several lines of code in
succession
-they are also used to prevent repeating lines/blocks of code that perform the same task/function


syntax:

function functionName(){
    code blocks (statement)
}

>> function keywqord 
-used to define a js functions.

>>funtionName
-function name . functions are named to be 
able to use later in the code.


>> codeblock/fucntion block ({})
- statements which comprise the body of the function. This is where the code
to be executed.



*/

function printName (){
    console.log('My name is Emmanuel');
}

/* fucntion invocation- it is common to use the term "call a function"
 instead of "invoke a function"

*/
printName();


declaredFunction();
// error : it is not yet defined.
function declaredFunction(){
    console.log('this is a defined function');
} 

/* 
Difference between: 
Function declaration vs Expression

A function can be created through funtion declaration by 
using function keyword + function name.

Declared Function are not executed immediately.
"They are saved for later use", and will be executed later.
when they are invoked (called)
*/


// 

function declaredFunction2(){
    console.log("Declared function");
}

declaredFunction2();
// declared functions can be declared multiple times

declaredFunction2();
declaredFunction2();
declaredFunction2();

//Expression
/* function expression 
can also be stored in a variable. this is called a function expression
is an anonymous*/

let variableFunction = function() 
{
   console.log('I am a variable func');
}

variableFunction();


/* 
We can alos function expression, we invoke it */

let functionExpression = function functionName(){
    console.log('Hello this iwth function name');
}


functionExpression();


//You can reassign declared fucntion and function expression to new 
// to new anonymous
//reassigned
declaredFunction = function(){
    console.log('Update declared function');
};

declaredFunction();


functionExpression = function(){
    console.log('Update  function expression');
};

functionExpression();


const constantFunction = function(){
    console.log('Initialized with const');
};

constantFunction();

/* constantFunction = function(){
    console.log('Cannot be reassigned');
};

constantFunction(); 

will not work because it is const*/




/* Function Scoping 


Scope is accessiblity (visibility)
of variables within our program
javaScript Variables has 3 types of scope:

1.local/block scope
2.global scope
3. function scope

*/

{
    let localVar = "Kim Seok-Jin";

    console.log(localVar); 
}

let globalVar = "The World Most Handsome";

console.log("");
/* // console.log(localVar); 

only visible within the function where it is defined*/
console.log(globalVar);

// global var has global scope which means can be defined anywhere in your JS code


/* Function Scope
 -e



*/
console.log("");
function showNames(){

    var functionVar = 'Jungkook';
    const functionConst = "BTS";
    let functionLet = 'kookie';

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
    
};

showNames();

/* console.log(functionVar);
console.log(functionConst);
console.log(functionLet);

cannot be defined outside the function*/

/* Nested Function */

/* You can create another function inside a function. This is called a ne
nested function. This nested func, being inside a new function will have accessvariable, 
name as they are within the same scope/codeblock */


function myNewFunc(){
    let name = 'Yor';

    function nestedFunc(){
        let nestedName = 'Xiyor';
        console.log(nestedName);
    };

    console.log(name);

nestedFunc();

};

myNewFunc();





console.log("");

/* let globalName = 'Thonie';

function myNewFunction2(){
    let name
} */

/* alert func 
alert()
syntax:
alert('message');*/

// alert("This is an alert");

/* function showSampleAlert(){
    alert("alert inside the a function")
}

showSampleAlert();

console.log("I will only show after the alert"); */

/* prompt func

syntax:
prompt('<dialog>')*/

/* let samplePrompt = prompt('Enter your name: ')
console.log("Hello " + samplePrompt);

let sampleNullPrompt = prompt('Dont input anything');

console.log(sampleNullPrompt); */
//if prompt is cancleed , the result will be null
// how ever if there is no input in the prompt, it will show a empty string.



/* function printWelcomeMessage(){
    let firstName = prompt('Enter you first Name: ');
    let lastName = prompt('Enter you last Name: ');

    console.log("Hello " + firstName + " " + lastName);
    console.log("Welcome to Gamer's Guild. ");
};
printWelcomeMessage(); */

/* Naming Convention:

            function should be definitive of it's task.
            usually contains verb



*/

//Array

function getCourses(){
    let course = ['PE','Programming 101','Math 203',' english'];
    console.log(course);
}

getCourses();




